package com.example.imd;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
    public void resultado(View vista){
    	// Esta funcion se encarga de enviar los datos y mostrar otro layout
    	// Obtenemos los datos
    	String nombre = ((TextView)findViewById(R.id.editText1)).getText().toString();
    	String peso = ((TextView)findViewById(R.id.editText2)).getText().toString();
    	String altura = ((TextView)findViewById(R.id.editText3)).getText().toString();
    	// Configuramos el bundle
    	 Bundle miBundle = new Bundle();
    	 miBundle.putString("nombre", nombre);
    	 miBundle.putString("peso", (peso+""));
    	 miBundle.putString("altura", (altura+""));

    	Intent vd = new Intent(this, Resultado.class); //Creamos la vista
    	
    	vd.putExtras(miBundle);//Le enviamos el bundle al intent (a la vista 'vd')
    	startActivity(vd);
    	
    }

    public void finalizar(View vista){
    	
    	finish();//Esta instruccion finaliza el hilo, cada vista debe tener una de estas
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
