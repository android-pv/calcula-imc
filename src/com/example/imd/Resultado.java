package com.example.imd;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class Resultado extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultado);

		// Recogemos los datos que llegaron a esta vista
		try {
			Bundle miBundle = this.getIntent().getExtras();
			String nombre_recibido = miBundle.getString("nombre");
			String peso_recibido = miBundle.getString("peso");
			String altura_recibido = miBundle.getString("altura");
			TextView campo_nombre = (TextView) findViewById(R.id.textView1);
			TextView campo_peso = (TextView) findViewById(R.id.textView2);
			TextView campo_altura = (TextView) findViewById(R.id.textView3);
			TextView campo_estado = (TextView) findViewById(R.id.textView7);
			campo_nombre.setText(nombre_recibido);
			campo_peso.setText(peso_recibido);
			campo_altura.setText(altura_recibido);
			campo_estado.setText(imc(Float.parseFloat(peso_recibido),
					Float.parseFloat(altura_recibido)));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public String imc(float peso, float altura) {
		float imc = peso / (altura * altura);
		if (imc < 18.5)
			return "Peso bajo";
		if (imc >= 18.5 && imc < 25)
			return "Peso normal";
		if (imc >= 25 && imc < 30)
			return "Sobrepeso";
		if (imc >= 30 && imc < 35)
			return "Obesidad I";
		if (imc >= 35 && imc < 40)
			return "Obesidad II";
		if (imc >= 40 && imc < 50)
			return "Obesidad III";
		// if (imc >= 50)
		return "Obesidad IV";
	}

	public void finalizar(View vista) {

		finish();// Esta instruccion finaliza el hilo, cada vista debe tener una
					// de estas
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.resultado, menu);
		return true;
	}

}
